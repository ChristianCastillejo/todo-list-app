# README

## Intro
This is a basic **todo-list app** that covers the most important features of any real world app.

**Frontend:**  
* React
*  React Router
* Redux
* Axios

**Backend:**  
* Ruby on Rails
* PostgreSQL

**Auth:**  
* JWT

You can find **more info** in: _docs/doc.pdf_

## Usage 

**Run the server:**

`$ cd todo-list-server`

`$ rails db:creater`

`$ rails db:migrate`

`$ http :4000/signup name=ash email=team@appic.com password=teamappic password_confirmation=teamappic`

`$ rails s -p 4000`

**Run the client:**

`$ cd todo-list-client`

`$ yarn install`

`$ yarn start`

_You need to run server and client at the same time._

## License

This code is distributed under the MIT license, see the LICENSE file.
