import { FETCH_LOGGED_IN_USER, LOGOUT } from "../actions/action_types";

export function loggedInUserReducer(state = {}, action) {
  switch (action.type) {
    case FETCH_LOGGED_IN_USER:
      return action.payload.data;
    case LOGOUT:
      return action.payload;
    default: {
      return state;
    }
  }
}
