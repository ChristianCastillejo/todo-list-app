import { combineReducers } from "redux";
import { todoListReducer } from "./todo_reducer.js";
import { loggedInUserReducer } from "./session_reducer.js";

const rootReducer = combineReducers({
  todoList: todoListReducer,
  loggedInUser: loggedInUserReducer
});
export default rootReducer;
