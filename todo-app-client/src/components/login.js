import React, { Component } from "react";
import { connect } from "react-redux";
import styles from "../assets/styles";
import _ from "lodash";

class Login extends Component {
  componentWillReceiveProps(nextProps) {
    if (!_.isEmpty(nextProps.loggedInUser)) {
      this.props.history.push("/");
    }
  }
  render() {
    return (
      <div style={styles.login}>
        <p>Please, login first.</p>
      </div>
    );
  }
}

const mapStateToProps = ({ loggedInUser }) => ({
  loggedInUser
});

export default connect(
  mapStateToProps,
  {}
)(Login);
