/* global window */
import React, { Component } from "react";
import { connect } from "react-redux";
import _ from "lodash";
import cookie from "react-cookies";
import { login, logout, fetchLoggedInUser } from "../actions/session_actions";
import { Button, Menu } from "semantic-ui-react";
import styles from "../assets/styles";
import logo from "../assets/logo.png";

class Layout extends Component {
  constructor(props) {
    super(props);
    this.state = { loggedInUser: this.props.loggedInUser };
  }

  componentDidMount() {
    const token = cookie.load("jwt");
    if (token) {
      this.props.fetchLoggedInUser();
    }
  }

  componentWillReceiveProps(nextProps) {
    if (!_.isEqual(this.props.loggedInUser, nextProps.loggedInUser)) {
      this.setState({ loggedInUser: nextProps.loggedInUser });
    }
  }

  render() {
    return (
      <div>
        <Menu secondary size="large" widths={7} style={styles.navbar}>
          <Menu.Item>
            <img src={logo} style={styles.logo} alt="logo" />
          </Menu.Item>
          {_.isEmpty(this.state.loggedInUser) ? (
            <Menu.Item position="right">
              <Button onClick={() => this.props.login()}>Login</Button>
            </Menu.Item>
          ) : (
            <React.Fragment>
              <Menu.Item position="right">{`Hello ${
                this.state.loggedInUser.email
              }`}</Menu.Item>
              <Menu.Item position="right">
                <Button onClick={() => this.props.logout()}>Log out</Button>
              </Menu.Item>
            </React.Fragment>
          )}
        </Menu>
        {this.props.children}
      </div>
    );
  }
}

const mapStateToProps = ({ loggedInUser }) => ({
  loggedInUser
});

export default connect(
  mapStateToProps,
  { login, logout, fetchLoggedInUser }
)(Layout);
