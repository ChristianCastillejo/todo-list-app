import React, { Component } from "react";
import { connect } from "react-redux";
import _ from "lodash";
import { Grid } from "semantic-ui-react";
import { fetchTodoList, addTodo, deleteTodo } from "../actions/index";
import TodoListView from "./todo_list_view";
import AddTodoView from "./add_todo_view";
import styles from "../assets/styles";

class TodoList extends Component {
  constructor(props) {
    super(props);
    this.state = {
      todoList: this.props.todoList,
      todo: { title: "" }
    };
    this.handleFieldChange = this.handleFieldChange.bind(this);
    this.addTodo = this.addTodo.bind(this);
    this.deleteTodo = this.deleteTodo.bind(this);
  }

  componentDidMount() {
    this.props.fetchTodoList();
  }

  componentWillReceiveProps(nextProps) {
    if (!_.isEqual(this.props.todoList, nextProps.todoList)) {
      this.setState({ todoList: nextProps.todoList });
    }
    if (_.isEmpty(nextProps.loggedInUser)) {
      this.props.history.push("/login");
    }
  }

  addTodo(todo) {
    if (!_.isEmpty(todo.title)) {
      this.props.addTodo(todo);
      this.setState({ todo: { title: "" } });
    }
  }

  deleteTodo(todo) {
    this.props.deleteTodo(todo);
  }

  handleFieldChange(title) {
    this.setState({
      todo: Object.assign({}, this.state.todo, { title })
    });
  }

  render() {
    return (
      <div style={styles.container}>
        <Grid>
          <Grid.Row columns={2} style={styles.todoListContainer}>
            <AddTodoView
              handleFieldChange={this.handleFieldChange}
              addTodo={this.addTodo}
              todo={this.state.todo}
            />
            <TodoListView
              todoList={this.state.todoList}
              deleteTodo={this.deleteTodo}
            />
          </Grid.Row>
        </Grid>
      </div>
    );
  }
}

const mapStateToProps = ({ todoList, loggedInUser }) => ({
  todoList,
  loggedInUser
});

const actions = {
  fetchTodoList,
  addTodo,
  deleteTodo
};

export default connect(
  mapStateToProps,
  actions
)(TodoList);
