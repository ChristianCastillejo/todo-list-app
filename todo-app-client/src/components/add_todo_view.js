import React from "react";
import { Grid, TextArea, Button } from "semantic-ui-react";
import styles from "../assets/styles";

const AddTodoView = ({ handleFieldChange, addTodo, todo }) => {
  return (
    <React.Fragment>
      <Grid.Column width={3} />
      <Grid.Column width={5} textAlign="center" style={styles.addTodo}>
        <TextArea
          size="mini"
          placeholder="Add a new todo..."
          onChange={event => handleFieldChange(event.target.value)}
          value={todo.title}
          style={styles.textArea}
        />
        <Button
          circular
          icon="add"
          style={styles.buttonAdd}
          onClick={() => addTodo(todo)}
        />
      </Grid.Column>
    </React.Fragment>
  );
};

export default AddTodoView;
