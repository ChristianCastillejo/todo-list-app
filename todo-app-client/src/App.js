import React from "react";
import { Provider } from "react-redux";
import {
  BrowserRouter as Router,
  Route,
  Redirect,
  Switch
} from "react-router-dom";
import "semantic-ui-css/semantic.min.css";

import Layout from "./components/layout";
import TodoList from "./components/todo_list";
import Login from "./components/login";
import store from "./store";
import cookie from "react-cookies";

function isUserAuthenticated() {
  const accessToken = cookie.load("jwt");
  return accessToken ? true : false;
}

const ProtectedRoute = ({ component: Component, ...rest }) => (
  <Route
    {...rest}
    render={props =>
      isUserAuthenticated() ? (
        <Component {...props} />
      ) : (
        <Redirect
          to={{
            pathname: "/login",
            state: { from: props.location }
          }}
        />
      )
    }
  />
);

const App = () => {
  return (
    <Provider store={store}>
      <Router>
        <Switch>
          <Layout>
            <ProtectedRoute exact path="/" component={TodoList} />
            <Route exact path="/login" component={Login} />
          </Layout>
        </Switch>
      </Router>
    </Provider>
  );
};

export default App;
