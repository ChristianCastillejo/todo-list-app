import axios from "axios";
import cookie from "react-cookies";

const axiosConfig = {
  baseURL: "http://localhost:4000",
  headers: {
    Accept: "application/json",
    "Content-Type": "application/json"
  }
};

export const axiosInstance = axios.create(axiosConfig);

const jwt = cookie.load("jwt");
if (jwt) {
  axiosInstance.defaults.headers.common.Authorization = `Auth Token ${jwt}`;
}
