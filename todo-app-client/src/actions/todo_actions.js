import { axiosInstance } from "./configured_axios";
import { FETCH_TODO_LIST } from "./action_types";

export function fetchTodoList() {
  const request = axiosInstance.get("/todos/");

  return {
    type: FETCH_TODO_LIST,
    payload: request
  };
}

export function addTodo(todo) {
  return dispatch => {
    axiosInstance.post("/todos/", todo).then(response => {
      dispatch(fetchTodoList());
    });
  };
}

export function deleteTodo(todo) {
  return dispatch => {
    axiosInstance.delete(`/todos/${todo}`).then(response => {
      dispatch(fetchTodoList());
    });
  };
}
